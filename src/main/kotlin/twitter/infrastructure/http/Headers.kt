package twitter.infrastructure.http

object Headers {
    const val CONTENT_TYPE = "Content-Type"
    const val APPLICATION_JSON = "application/json"
}