package twitter.infrastructure.http.getfollowers

import com.google.gson.Gson
import spark.Request
import spark.Response
import spark.Spark.get
import twitter.actions.GetFollowers
import twitter.infrastructure.http.Headers
import twitter.infrastructure.http.Params
import twitter.infrastructure.http.RouteHandler
import twitter.infrastructure.http.StatusCode

class GetFollowersHandler(gson: Gson, val action: GetFollowers): RouteHandler(gson) {
    private val URL = "twitter/:nickname/followers"

    override fun execute(request: Request, response: Response) {
        val followers = action.execute(request.params(Params.NICKNAME))
        response.status(StatusCode.OK)
        response.body(gson.toJson(followers))
    }

    override fun register() {
        get(URL, Headers.APPLICATION_JSON, this)
    }
}
