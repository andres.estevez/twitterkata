package twitter.infrastructure.http.follow

import com.google.gson.Gson
import spark.Request
import spark.Response
import spark.Spark.post
import twitter.actions.FollowUser
import twitter.actions.FollowUserData
import twitter.infrastructure.http.Headers
import twitter.infrastructure.http.Params
import twitter.infrastructure.http.RouteHandler
import twitter.infrastructure.http.StatusCode

class FollowHandler(gson: Gson, val action: FollowUser): RouteHandler(gson) {
    private val URL = "/twitter/:nickname/follow"

    override fun execute(request: Request, response: Response)  {
        var follower = request.params(Params.NICKNAME)
        val body = parse<FollowRequest>(request.body())
        action.execute(FollowUserData(follower, body.nickname))
        response.status(StatusCode.OK)
    }

    override fun register() {
        post(URL, Headers.APPLICATION_JSON, this)
    }
}