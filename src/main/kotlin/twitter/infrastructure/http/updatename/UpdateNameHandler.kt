package twitter.infrastructure.http.updatename

import com.google.gson.Gson
import spark.Request
import spark.Response
import spark.Spark.post
import twitter.actions.UpdateName
import twitter.actions.UpdateNameData
import twitter.infrastructure.http.Headers
import twitter.infrastructure.http.Params
import twitter.infrastructure.http.RouteHandler
import twitter.infrastructure.http.StatusCode

class UpdateNameHandler(gson: Gson, val action: UpdateName) : RouteHandler(gson) {
    private val URL = "twitter/:nickname/updateName"

    override fun execute(request: Request, response: Response) {
        val nickname = request.params(Params.NICKNAME)
        val body = parse<UpdateNameRequest>(request.body())
        action.execute(UpdateNameData(nickname, body.newName))
        response.status(StatusCode.OK)
    }

    override fun register() {
        post(URL, Headers.APPLICATION_JSON, this)
    }
}