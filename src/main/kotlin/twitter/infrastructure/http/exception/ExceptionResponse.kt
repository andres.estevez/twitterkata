package twitter.infrastructure.http.exception

class ExceptionResponse(val code: String) {
    override fun equals(other: Any?): Boolean {
        val o = other as ExceptionResponse
        return o.code == code
    }
}