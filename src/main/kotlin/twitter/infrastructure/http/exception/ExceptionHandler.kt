package twitter.infrastructure.http.exception

import com.google.gson.Gson
import spark.Request
import spark.Response
import spark.Spark.exception
import twitter.infrastructure.http.RouteHandler
import twitter.infrastructure.http.StatusCode

class ExceptionHandler<T>(
        gson: Gson,
        private val cls: Class<T>,
        private val statusCode: Int = StatusCode.BAD_REQUEST)
            : RouteHandler(gson) where T : Exception {

    private val EMPTY = ""
    private val EXCEPTION_SUFFIX = "Exception"

    override fun execute(request: Request, response: Response) {
        response.body(gson.toJson(buildResponse()))
        response.status(statusCode)
    }

    private fun buildResponse(): ExceptionResponse {
        return ExceptionResponse(cls.simpleName.replace(EXCEPTION_SUFFIX, EMPTY))
    }

    override fun register() {
        exception(cls) { _, req, res -> execute(req, res) }
    }
}