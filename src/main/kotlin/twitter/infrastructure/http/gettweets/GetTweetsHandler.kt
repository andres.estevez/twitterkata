package twitter.infrastructure.http.gettweets

import com.google.gson.Gson
import spark.Request
import spark.Response
import spark.Spark.get
import twitter.actions.GetTweets
import twitter.infrastructure.http.Headers
import twitter.infrastructure.http.RouteHandler
import twitter.infrastructure.http.StatusCode

class GetTweetsHandler(gson: Gson, val action: GetTweets) : RouteHandler(gson) {
    private val URL = "/users/:nickname/tweets"

    override fun execute(request: Request, response: Response) {
        val nickname = getNicknameFromParams(request)
        val tweets = action.execute(nickname).map { TweetResponse(it.nickname, it.text, it.time) }
        response.body(gson.toJson(tweets))
        response.status(StatusCode.OK)
    }

    override fun register() {
        get(URL, Headers.APPLICATION_JSON, this)
    }

}
