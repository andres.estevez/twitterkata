package twitter.infrastructure.http

object StatusCode {
    const val OK = 200
    const val BAD_REQUEST = 400
    const val UNAUTHORIZED = 401
}