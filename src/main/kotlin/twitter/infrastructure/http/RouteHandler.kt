package twitter.infrastructure.http

import com.google.gson.Gson
import spark.Request
import spark.Response
import spark.Route

abstract class RouteHandler(val gson: Gson) : Route {

    override fun handle(request: Request, response: Response): String? {
        execute(request, response)
        response.header(Headers.CONTENT_TYPE, Headers.APPLICATION_JSON)
        return response.body() ?: ""
    }

    protected abstract fun execute(request: Request, response: Response);
    abstract fun register();

    protected inline fun <reified T> parse(json: String): T {
        return gson.fromJson<T>(json, T::class.java)
    }

    protected fun getNicknameFromParams(request: Request): String {
        return request.params(Params.NICKNAME)
    }
}