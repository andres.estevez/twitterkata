package twitter.infrastructure.http.tweet

import com.google.gson.Gson
import spark.Request
import spark.Response
import spark.Spark.post
import twitter.actions.TweetData
import twitter.actions.WriteTweet
import twitter.infrastructure.http.Headers
import twitter.infrastructure.http.RouteHandler
import twitter.infrastructure.http.StatusCode

class TweetHandler(gson: Gson, val action: WriteTweet): RouteHandler(gson) {
    private val URL = "/users/:nickname/tweet"

    override fun execute(request: Request, response: Response) {
        val nickname = getNicknameFromParams(request)
        val body = parse<TweetRequest>(request.body())
        action.execute(TweetData(nickname, body.text))
        response.status(StatusCode.OK)
    }

    override fun register() {
        post(URL, Headers.APPLICATION_JSON, this)
    }

}
