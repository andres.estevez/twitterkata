package twitter.infrastructure.http.login

import com.google.gson.Gson
import spark.Request
import spark.Response
import spark.Spark.post
import twitter.actions.Credentials
import twitter.actions.Login
import twitter.infrastructure.http.Headers
import twitter.infrastructure.http.RouteHandler
import twitter.infrastructure.http.StatusCode

class LoginHandler(gson: Gson, private val action: Login) : RouteHandler(gson) {
    override fun execute(request: Request, response: Response) {
        var credentials = parse<Credentials>(request.body())
        action.execute(credentials)
        response.status(StatusCode.OK)
    }

    override fun register() {
        post("/login", Headers.APPLICATION_JSON, this)
    }
}