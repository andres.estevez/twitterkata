package twitter.infrastructure.http.register

import com.google.gson.Gson
import spark.Request
import spark.Response
import spark.Spark.post
import twitter.actions.RegisterUser
import twitter.actions.RegistrationData
import twitter.infrastructure.http.Headers
import twitter.infrastructure.http.RouteHandler
import twitter.infrastructure.http.StatusCode

class RegisterUserHandler(gson: Gson, val action: RegisterUser) : RouteHandler(gson) {
    private val URL = "/register"

    override fun execute(request: Request, response: Response) {
        val body = parse<RegisterUserRequest>(request.body())
        action.execute(RegistrationData(body.name, body.nickname))
        response.status(StatusCode.OK)
    }

    override fun register() {
        post(URL, Headers.APPLICATION_JSON, this)
    }
}