package twitter.infrastructure

import twitter.domain.Time

class SystemTime : Time {
    override fun millis(): Long {
        return System.currentTimeMillis()
    }

}