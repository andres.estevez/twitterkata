package twitter.infrastructure.persistence

import io.lettuce.core.api.sync.RedisCommands
import twitter.domain.IdGenerator

class RedisIdGenerator(private val client: RedisCommands<String, String?>, private val key: String) : IdGenerator {
    private val FIRST_ID = "1"

    override fun next(): Long {
        val id = client.get(key) ?: FIRST_ID
        return id.toLong()
    }

    override fun increment() {
        val id = next()
        client.set(key, (id + 1).toString())
    }
}