package twitter.infrastructure.persistence

class UserData(val nickname: String, val name: String) {
    override fun equals(other: Any?): Boolean {
        val o = other as UserData
        return o.name == name && o.nickname == nickname
    }
}