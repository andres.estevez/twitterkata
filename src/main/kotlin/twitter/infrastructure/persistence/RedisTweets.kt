package twitter.infrastructure.persistence

import com.google.gson.Gson
import io.lettuce.core.api.sync.RedisCommands
import twitter.domain.Tweet
import twitter.domain.Tweets

class RedisTweets(val client: RedisCommands<String, String?>) : Tweets {
    private val gson = Gson()

    override fun save(tweet: Tweet) {
        val data = tweetToTweetData(tweet)
        client.hset(userTweetsKey(tweet.nickname), tweetKey(tweet), gson.toJson(data))
    }

    fun userTweetsKey(nickname: String): String {
        return "user:${nickname.toLowerCase()}:tweets"
    }

    override fun getAllOf(nickname: String): List<Tweet> {
        val tweets = getUserTweetsData(nickname)
        return tweets.map { tweetDataToTweet(it) }
    }

    private fun getUserTweetsData(nickname: String): List<TweetData> {
        return client.hgetall(userTweetsKey(nickname)).map { gson.fromJson(it.value, TweetData::class.java) }
    }

    fun tweetKey(tweet: Tweet): String {
        return tweet.id.toString()
    }

    private fun tweetDataToTweet(data: TweetData): Tweet {
        return Tweet(data.id, data.nickname, data.text, data.time)
    }

    private fun tweetToTweetData(data: Tweet): TweetData {
        return TweetData(data.id, data.nickname, data.text, data.time)
    }
}
