package twitter.infrastructure.persistence

import com.google.gson.Gson
import io.lettuce.core.api.sync.RedisCommands
import io.lettuce.core.api.sync.RedisStringCommands
import twitter.domain.User
import twitter.domain.Users


class RedisUsers(val redisClient: RedisCommands<String, String?>) : Users {
    val USERS_KEY = "users"

    override fun exists(nickname: String): Boolean {
        val user = redisClient.hget(USERS_KEY, userKey(nickname))
        return user != null
    }

    override fun get(nickname: String): User? {
        val user = redisClient.hget(USERS_KEY, nickname)
        if (user != null)
            return deserializeUser(user)

        return null
    }

    override fun save(user: User) {
        redisClient.hset(USERS_KEY, userKey(user.nickname), serializeUser(user))
    }

    private fun serializeUser(user: User) = Gson().toJson(UserData(user.nickname, user.name))

    private fun deserializeUser(user: String): User? {
        val data = Gson().fromJson<UserData>(user, UserData::class.java)
        return User(data.name, data.nickname)
    }

    private fun userKey(nickname: String) = nickname.toLowerCase()
}