package twitter.actions

class RegistrationData(val name: String, val nickname: String) {
    override fun equals(other: Any?): Boolean {
        val o = other as RegistrationData
        return o.name == name && o.nickname == nickname
    }
}
