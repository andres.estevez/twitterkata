package twitter.actions

import twitter.domain.User
import twitter.domain.UserAlreadyExistsException
import twitter.domain.Users

class RegisterUser(private val users: Users) {

    fun execute(registration: RegistrationData): UserCreated {
        if (users.exists(registration.nickname))
            throw UserAlreadyExistsException();

        val user = User(registration.name, registration.nickname);
        users.save(user);

        return UserCreated(user.name, user.nickname);
    }
}
