package twitter.actions

class Credentials(val nickname: String, val password: String) {
    override fun equals(other: Any?): Boolean {
        val o = other as Credentials
        return o.nickname == nickname && o.password == password
    }
}