package twitter.actions

import twitter.domain.FollowedNotFoundException
import twitter.domain.FollowerNotFoundException
import twitter.domain.Users

class FollowUser(val users: Users) {
    fun execute(follow: FollowUserData) {
        if (!users.exists(follow.follower))
            throw FollowerNotFoundException()

        var followed = users.get(follow.followed)
        if (followed == null)
            throw FollowedNotFoundException()

        followed.addFollower(follow.follower)
        users.save(followed)
    }

}