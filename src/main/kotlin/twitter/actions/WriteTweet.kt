package twitter.actions

import twitter.domain.*

class WriteTweet(
        private val users: Users,
        private val tweets: Tweets,
        private val idGenerator: IdGenerator,
        private val time: Time) {
    fun execute(tweetData: TweetData) {
        val user = users.find(tweetData.nickname)
        val id = idGenerator.next()
        val tweet = Tweet(id, user.nickname, tweetData.text, time.millis())
        tweets.save(tweet)
        idGenerator.increment()
    }

}
