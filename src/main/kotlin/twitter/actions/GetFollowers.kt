package twitter.actions

import twitter.domain.Users
import twitter.domain.find

class GetFollowers(val users: Users) {
    fun execute(nickname: String): List<String> {
        return users.find(nickname).followers
    }

}
