package twitter.actions

import twitter.domain.Users
import twitter.domain.find

class UpdateName(private val users: Users) {
    fun execute(update: UpdateNameData) {
        var user = users.find(update.nickname);
        user.name = update.newName
        users.save(user)
    }

}
