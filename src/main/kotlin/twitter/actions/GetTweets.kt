package twitter.actions

import twitter.domain.Tweet
import twitter.domain.Tweets
import twitter.domain.Users
import twitter.domain.checkExists

class GetTweets(val users: Users, val tweets: Tweets) {
    fun execute(nickname: String): List<Tweet> {
        users.checkExists(nickname)
        return tweets.getAllOf(nickname)
    }
}
