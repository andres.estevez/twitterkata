package twitter.actions

import twitter.domain.InvalidCredentialsException
import twitter.domain.Users
import twitter.domain.checkExists

class Login(val users: Users) {
    fun execute(credentials: Credentials) {
        if (!users.exists(credentials.nickname))
            throw InvalidCredentialsException()
    }

}
