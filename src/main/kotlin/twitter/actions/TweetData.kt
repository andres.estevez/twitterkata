package twitter.actions

class TweetData(val nickname: String, val text: String) {
    override fun equals(other: Any?): Boolean {
        val o = other as TweetData
        return o.nickname == nickname && o.text == text
    }
}
