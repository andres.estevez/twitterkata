package twitter.actions

class FollowUserData(val follower: String, val followed: String) {
    override fun equals(other: Any?): Boolean {
        val o = other as FollowUserData
        return o.followed == followed && o.follower == follower
    }
}
