package twitter.actions

class UpdateNameData(val nickname: String, val newName: String) {
    override fun equals(other: Any?): Boolean {
        val o = other as UpdateNameData
        return o.newName == newName && o.nickname == nickname
    }
}
