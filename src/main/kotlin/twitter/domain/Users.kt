package twitter.domain

interface Users {
    fun exists(nickname: String): Boolean
    fun get(nickname: String): User?
    fun save(user: User)
}