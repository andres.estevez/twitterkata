package twitter.domain

fun Users.checkExists(nickname: String) {
    if (!exists(nickname))
        throw UserNotFoundException()
}

fun Users.find(nickname: String): User {
    return get(nickname) ?: throw UserNotFoundException()
}