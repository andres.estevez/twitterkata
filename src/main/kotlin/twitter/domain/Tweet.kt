package twitter.domain

class Tweet(val id: Long, val nickname: String, val text: String, val time: Long) {
    override fun equals(other: Any?): Boolean {
        val o = other as Tweet
        return o.id == id
    }
}