package twitter.domain

interface Tweets {
    fun save(tweet: Tweet)
    fun getAllOf(nickname: String): List<Tweet>
}
