package twitter.domain

interface IdGenerator {
    fun next(): Long
    fun increment()
}