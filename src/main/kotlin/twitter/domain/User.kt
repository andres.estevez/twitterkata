package twitter.domain

class User(var name: String, val nickname: String) {
    var followers: ArrayList<String> = ArrayList()

    fun addFollower(nickname: String): User {
        followers.add(nickname)
        return this
    }
}