package twitter.domain

interface Time {
    fun millis(): Long
}