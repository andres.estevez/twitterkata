import com.google.gson.Gson
import io.lettuce.core.RedisClient
import redis.embedded.RedisServer
import twitter.actions.*
import twitter.domain.*
import twitter.infrastructure.SystemTime
import twitter.infrastructure.http.StatusCode
import twitter.infrastructure.http.exception.ExceptionHandler
import twitter.infrastructure.http.follow.FollowHandler
import twitter.infrastructure.http.getfollowers.GetFollowersHandler
import twitter.infrastructure.http.gettweets.GetTweetsHandler
import twitter.infrastructure.http.login.LoginHandler
import twitter.infrastructure.http.register.RegisterUserHandler
import twitter.infrastructure.http.tweet.TweetHandler
import twitter.infrastructure.http.updatename.UpdateNameHandler
import twitter.infrastructure.persistence.RedisIdGenerator
import twitter.infrastructure.persistence.RedisTweets
import twitter.infrastructure.persistence.RedisUsers

fun main(args: Array<String>) {
    val PORT = 6387
    val REDIS_URL = "redis://localhost:${PORT}"

    val server = RedisServer(PORT)
    server.start()
    val client = RedisClient.create(REDIS_URL).connect().sync()

    val users = RedisUsers(client)
    val tweets = RedisTweets(client)
    val idGenerator = RedisIdGenerator(client, "tweet_id");
    val time = SystemTime()

    val gson = Gson()

    val handlers = arrayOf(
        LoginHandler(gson, Login(users)),
        RegisterUserHandler(gson, RegisterUser(users)),
        UpdateNameHandler(gson, UpdateName(users)),
        FollowHandler(gson, FollowUser(users)),
        GetFollowersHandler(gson, GetFollowers(users)),
        TweetHandler(gson, WriteTweet(users, tweets, idGenerator, time)),
        GetTweetsHandler(gson, GetTweets(users, tweets)),
        ExceptionHandler(gson, UserNotFoundException::class.java),
        ExceptionHandler(gson, InvalidCredentialsException::class.java, StatusCode.UNAUTHORIZED),
        ExceptionHandler(gson, FollowerNotFoundException::class.java),
        ExceptionHandler(gson, FollowedNotFoundException::class.java),
        ExceptionHandler(gson, UserAlreadyExistsException::class.java)
    )

    handlers.forEach { it.register() }
}