package twitter.infrastructure.persistence

import com.google.gson.Gson
import io.lettuce.core.RedisClient
import io.lettuce.core.api.sync.RedisCommands
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import redis.embedded.RedisServer
import twitter.domain.Tweet
import twitter.domain.Tweets

class RedisTweetsShould {
    private val PORT = 6389
    private val REDIS_URL = "redis://localhost:${PORT}"
    private lateinit var server: RedisServer
    private lateinit var client: RedisCommands<String, String?>

    private lateinit var tweets: RedisTweets

    private val ID = 1L
    private val NICKNAME = "fenix"
    private val TEXT = "text1"
    private val TIME: Long = 1
    private val TWEET = Tweet(ID, NICKNAME, TEXT, TIME)
    private val TWEET_DATA = TweetData(ID, NICKNAME, TEXT, TIME)

    @BeforeEach
    fun setup() {
        server = RedisServer(PORT)
        server.start()
        client = RedisClient.create(REDIS_URL).connect().sync()
        tweets = RedisTweets(client)
    }

    @AfterEach
    fun end() {
        server.stop()
    }

    @Test
    fun `save tweets`() {
        // When
        tweets.save(TWEET)

        // Then
        val persisted = client.hget(tweets.userTweetsKey(NICKNAME), ID.toString())
        assertThat(persisted).isEqualTo(Gson().toJson(TWEET_DATA))
    }

    @Test
    fun `return tweets of user by nickname`() {
        // Given
        tweets.save(TWEET)

        // When
        val userTweets = tweets.getAllOf(NICKNAME)

        // Then
        assertThat(userTweets).isEqualTo(listOf(TWEET))
    }
}