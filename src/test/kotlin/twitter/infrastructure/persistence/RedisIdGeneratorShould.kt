package twitter.infrastructure.persistence

import io.lettuce.core.RedisClient
import io.lettuce.core.api.sync.RedisCommands
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import redis.embedded.RedisServer
import twitter.domain.Tweet

class RedisIdGeneratorShould {
    private val PORT = 6389
    private val REDIS_URL = "redis://localhost:${PORT}"
    private lateinit var server: RedisServer
    private lateinit var client: RedisCommands<String, String?>

    private val KEY = "KEY"
    private lateinit var generator: RedisIdGenerator

    @BeforeEach
    fun setup() {
        server = RedisServer(PORT)
        server.start()
        client = RedisClient.create(REDIS_URL).connect().sync()
        generator = RedisIdGenerator(client, KEY)
    }

    @AfterEach
    fun end() {
        server.stop()
    }

    @Test
    fun `return first id if first call to next`() {
        // When
        val id = generator.next()

        assertThat(id).isEqualTo(1)
    }

    @Test
    fun `increment id`() {
        // Given
        val id = generator.next()

        // When
        generator.increment()
        val next = generator.next()

        // Then
        assertThat(next).isEqualTo(id + 1)
    }
}