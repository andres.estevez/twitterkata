package twitter.infrastructure.persistence

import com.google.gson.Gson
import io.lettuce.core.RedisClient
import io.lettuce.core.api.sync.RedisCommands
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.*
import redis.embedded.RedisServer
import twitter.domain.User


class RedisUsersShould {
    private val PORT = 6389
    private val REDIS_URL = "redis://localhost:${PORT}"

    private val NICKNAME = "fenix"
    private val NAME = "ikki"
    private val NAME2 = "jhun"
    private val USER = User(NAME, NICKNAME)
    private val USER_DATA = UserData(NICKNAME, NAME)

    private val USERS_KEY = "users"

    private lateinit var server: RedisServer
    private lateinit var client: RedisCommands<String, String?>

    @BeforeEach
    fun setup() {
        server = RedisServer(PORT)
        server.start()
        client = RedisClient.create(REDIS_URL).connect().sync()
    }

    @AfterEach
    fun end() {
        server.stop()
    }

    @Test
    fun `check if user exists`() {
        // Given
        val users = RedisUsers(client)
        client.hset(users.USERS_KEY, NICKNAME, Gson().toJson(USER_DATA))

        // When
        val exist = users.exists(NICKNAME)

        // Then
        assertThat(exist).isEqualTo(true)
    }

    @Test
    fun `return existing user`() {
        // Given
        val users = RedisUsers(client)
        client.hset(users.USERS_KEY, NICKNAME, Gson().toJson(USER_DATA))

        // When
        val user = users.get(NICKNAME)

        // Then
        assertThat(user!!.nickname).isEqualTo(USER.nickname)
        assertThat(user.name).isEqualTo(USER.name)
    }

    @Test
    fun `return null if user not exist`() {
        // Given
        val users = RedisUsers(client)

        // When
        val user = users.get(NICKNAME)

        // Then
        assertThat(user).isEqualTo(null)
    }

    @Test
    fun `add new user`() {
        // Given
        val users = RedisUsers(client)

        // When
        users.save(USER)

        // Then
        assertUserInRedis(USER)
    }

    @Test
    fun `update existing user`() {
        // Given
        val users = RedisUsers(client)
        client.hset(USERS_KEY, USER.nickname, Gson().toJson(USER_DATA))

        // When
        val user = User(NAME2, NICKNAME)
        users.save(user)

        // Then
        assertUserInRedis(user)
    }

    private fun assertUserInRedis(user: User) {
        var jsonUser = client.hget(USERS_KEY, user.nickname)
        assertThat(jsonUser).isEqualTo(Gson().toJson(UserData(user.nickname, user.name)))
    }
}