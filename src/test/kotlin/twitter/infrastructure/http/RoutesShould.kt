package twitter.infrastructure.http

import com.google.gson.Gson
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import spark.Request
import spark.Response
import twitter.actions.*
import twitter.domain.InvalidCredentialsException
import twitter.domain.Tweet
import twitter.domain.User
import twitter.infrastructure.http.exception.ExceptionHandler
import twitter.infrastructure.http.exception.ExceptionResponse
import twitter.infrastructure.http.follow.FollowHandler
import twitter.infrastructure.http.follow.FollowRequest
import twitter.infrastructure.http.getfollowers.GetFollowersHandler
import twitter.infrastructure.http.gettweets.GetTweetsHandler
import twitter.infrastructure.http.gettweets.TweetResponse
import twitter.infrastructure.http.login.LoginHandler
import twitter.infrastructure.http.register.RegisterUserHandler
import twitter.infrastructure.http.register.RegisterUserRequest
import twitter.infrastructure.http.tweet.TweetHandler
import twitter.infrastructure.http.tweet.TweetRequest
import twitter.infrastructure.http.updatename.UpdateNameHandler
import twitter.infrastructure.http.updatename.UpdateNameRequest

class RoutesShould {
    private val gson = Gson()
    private val NICKNAME = "FENIX"

    private val NAME = "IKKI"
    private val NEWNAME = "JHUN"
    private val FOLLOWED_NICKNAME = "ESMERALDA"

    private val FOLLOWED_NAME = "LA RUBIA DE LA ISLA DE LA MUERTE"
    private val FOLLOWED_USER = User(FOLLOWED_NAME, FOLLOWED_NICKNAME)
    private val USER = User(NAME, NICKNAME)
    private val ID1 = 1L
    private val TEXT = "TEXT1"

    private val REGISTER_USER_REQUEST = RegisterUserRequest(NAME, NICKNAME)

    private val UPDATE_USER_NAME_REQUEST = UpdateNameRequest(NEWNAME)
    private val FOLLOW_REQUEST = FollowRequest(FOLLOWED_NICKNAME)
    private val TWEET_REQUEST = TweetRequest(TEXT)

    private val CREDENTIALS = Credentials(NICKNAME, NICKNAME)

    private lateinit var response: Response;
    private val once = times(1)

    @BeforeEach
    fun setup() {
        response = mock();
    }

    @Test
    fun `respond OK if register user`() {
        // Given
        val request = mock<Request>() {
            on { body() } doReturn gson.toJson(REGISTER_USER_REQUEST)
        }

        val action = mock<RegisterUser>()

        // When
        RegisterUserHandler(Gson(), action).handle(request, response)

        // Then
        verifyResponseOK()
        verify(action, once).execute(RegistrationData(NAME, NICKNAME))
    }

    private fun verifyResponseOK() {
        verify(response, once).status(StatusCode.OK);
    }

    @Test
    fun `respond OK if update user name`() {
        // Given
        val request = mock<Request>() {
            on { params(Params.NICKNAME) } doReturn NICKNAME
            on { body() } doReturn gson.toJson(UPDATE_USER_NAME_REQUEST)
        }

        val action = mock<UpdateName>()

        // When
        UpdateNameHandler(Gson(), action).handle(request, response)

        // Then
        verifyResponseOK()
        verify(action, once).execute(UpdateNameData(NICKNAME, NEWNAME))
    }

    @Test
    fun `respond OK if follow user`() {
        // Given
        val request = mock<Request>() {
            on { params(Params.NICKNAME) } doReturn NICKNAME
            on { body() } doReturn gson.toJson(FOLLOW_REQUEST)
        }

        val action = mock<FollowUser>()

        // When
        FollowHandler(Gson(), action).handle(request, response)

        // Then
        verifyResponseOK()
        verify(action, once).execute(FollowUserData(NICKNAME, FOLLOWED_NICKNAME))
    }

    @Test
    fun `respond list of followers nicknames if get followers`() {
        // Given
        val request = mock<Request>() {
            on { params(Params.NICKNAME) } doReturn FOLLOWED_NICKNAME
        }

        val action = mock<GetFollowers> {
            on { execute(FOLLOWED_NICKNAME) } doReturn listOf(NICKNAME)
        }

        // When
        GetFollowersHandler(Gson(), action).handle(request, response)

        // Then
        verifyResponseOK()
        verify(action, once).execute(FOLLOWED_NICKNAME)
        verifyResponseBody(listOf(NICKNAME))
    }

    private fun <T> verifyResponseBody(obj: T) {
        verify(response, once).body(gson.toJson(obj))
    }

    @Test
    fun `respond OK if user tweets`() {
        // Given
        val request = mock<Request>() {
            on { params(Params.NICKNAME) } doReturn NICKNAME
            on { body() } doReturn gson.toJson(TWEET_REQUEST)
        }

        val action = mock<WriteTweet>()

        // When
        TweetHandler(Gson(), action).handle(request, response)

        // Then
        verifyResponseOK()
        verify(action, once).execute(TweetData(NICKNAME, TEXT))
    }

    @Test
    fun `respond OK if user login`() {
        // Given
        val request = mock<Request>() {
            on { params(Params.NICKNAME) } doReturn NICKNAME
            on { body() } doReturn gson.toJson(CREDENTIALS)
        }

        val action = mock<Login>()

        // When
        LoginHandler(Gson(), action).handle(request, response)

        // Then
        verifyResponseOK()
        verify(action, once).execute(Credentials(NICKNAME, NICKNAME))
    }

    @Test
    fun `respond list of tweets from user nickname`() {
        // Given
        val request = mock<Request>() {
            on { params(Params.NICKNAME) } doReturn NICKNAME
        }

        val time = 1L
        val tweet = Tweet(ID1, NICKNAME, TEXT, time)

        val action = mock<GetTweets> {
            on { execute(NICKNAME) } doReturn listOf(tweet)
        }

        // When
        GetTweetsHandler(Gson(), action).handle(request, response)

        // Then
        verifyResponseOK()
        verify(action, once).execute(NICKNAME)

        val listOfTweetsResponse = listOf(TweetResponse(NICKNAME, TEXT, time))
        verifyResponseBody(listOfTweetsResponse)
    }

    @Test
    fun `return error code on domain exception`() {
        val handler = ExceptionHandler(gson, InvalidCredentialsException::class.java)

        handler.handle(mock(), response)

        verifyResponseBody(ExceptionResponse("InvalidCredentials"))
    }
}