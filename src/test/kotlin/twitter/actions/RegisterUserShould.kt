package twitter.actions

import com.nhaarman.mockitokotlin2.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import twitter.domain.UserAlreadyExistsException
import twitter.domain.Users

class RegisterUserShould {

    private val ID: String = "1";
    private val NAME: String = "IKKI";
    private val NICKNAME: String = "FENIX";

    private val REGISTRATION_DATA: RegistrationData = RegistrationData(NAME, NICKNAME);
    private val USER_CREATED: UserCreated = UserCreated(NAME, NICKNAME);

    private val USERS_CONTAINING_NICKNAME = mock<Users>(){
        on { exists(any())} doReturn true;
    };

    private val USERS = mock<Users>(){
        on { exists(any())} doReturn false;
    }

    @Test
    fun `throw an error if user already exist`() {
        // Given
        var registerUser = RegisterUser(USERS_CONTAINING_NICKNAME);

        assertThrows<UserAlreadyExistsException>
        {
            registerUser.execute(REGISTRATION_DATA)
        }
    }

    @Test
    fun `return a new user created`() {
        // Given
        var registerUser = RegisterUser(USERS);

        // When
        var created = registerUser.execute(REGISTRATION_DATA);

        // Then
        assertThat(created).isEqualTo(USER_CREATED);
    }

    @Test
    fun `add user to users`() {
        // Given
        var registerUser = RegisterUser(USERS);

        // When
        registerUser.execute(REGISTRATION_DATA);

        // Then
        verify(USERS, times(1)).save(any());
    }
}