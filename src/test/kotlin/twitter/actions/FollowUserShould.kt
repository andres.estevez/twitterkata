package twitter.actions

import com.nhaarman.mockitokotlin2.*
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import twitter.domain.FollowedNotFoundException
import twitter.domain.FollowerNotFoundException
import twitter.domain.User
import twitter.domain.Users

class FollowUserShould {
    private val FOLLOWER = "IKKI"
    private val FOLLOWED = "ESMERALDA"
    private val FOLLOWED_NAME = "RUBIA DE LA ISLA DE LA MUERTE"

    private val FOLLOW_USER_DATA = FollowUserData(FOLLOWER, FOLLOWED)

    private val USERS_WITHOUT_FOLLOWER = mock<Users>(){
        on { exists(any())} doReturn false;
    };

    private val USERS_WITHOUT_FOLLOWED = mock<Users>(){
        on { exists(any())} doReturn true
        on { get(any()) } doReturn null
    };

    private val USERS_WITH_FOLLOWED = mock<Users>(){
        on { exists(any())} doReturn true
        on { get(any()) } doReturn User(FOLLOWED_NAME, FOLLOWED)
    };

    @Test
    fun `throw exception if follower not exist`() {
        // Given
        val followUser = FollowUser(USERS_WITHOUT_FOLLOWER)

        assertThrows<FollowerNotFoundException> {
            // When
            followUser.execute(FOLLOW_USER_DATA)
        }
    }

    @Test
    fun `throw exception if followed not found`() {
        // Given
        val followUser = FollowUser(USERS_WITHOUT_FOLLOWED)

        assertThrows<FollowedNotFoundException> {
            // When
            followUser.execute(FOLLOW_USER_DATA)
        }
    }

    @Test
    fun `add follower to user`() {
        // Given
        val followUser = FollowUser(USERS_WITH_FOLLOWED)

        // When
        followUser.execute(FOLLOW_USER_DATA)

        // Then
        verifyUserSaved(FOLLOWED, FOLLOWER)
    }

    private fun verifyUserSaved(followed: String, follower: String) {
        val captor: KArgumentCaptor<User> = argumentCaptor { }
        verify(USERS_WITH_FOLLOWED, times(1)).save(captor.capture())
        Assertions.assertThat(captor.firstValue.nickname).isEqualTo(followed);
        Assertions.assertThat(captor.firstValue.followers).contains(follower);
    }
}