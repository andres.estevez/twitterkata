package twitter.actions

import com.nhaarman.mockitokotlin2.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import twitter.domain.*
import twitter.infrastructure.SystemTime

class WriteTweetShould {
    private val NICKNAME = "Fenix"
    private val NAME = "IKKI"
    private val USER = User(NAME, NICKNAME)
    private val ID = 1L
    private val TEXT = "TEXT"
    private val TIME = 1L
    private val TWEET_DATA = TweetData(NICKNAME, TEXT)
    private val TWEET = Tweet(ID, NICKNAME, TEXT, TIME)

    @Test
    fun `throw exception if user not exist`() {
        // Given
        val users = mock<Users> {
            on { exists(any()) } doReturn false
        }
        val tweet = WriteTweet(users, mock(), mock(), SystemTime())

        assertThrows<UserNotFoundException> { tweet.execute(TWEET_DATA) }
    }

    @Test
    fun `add a new tweet`() {
        val tweets = mock<Tweets>()
        val tweet = givenValidTweetAction(tweets)

        // When
        tweet.execute(TWEET_DATA)

        // Then
        verify(tweets, times(1)).save(TWEET)
    }

    private fun givenValidTweetAction(tweets: Tweets): WriteTweet {
        val users = mock<Users> {
            on { exists(any()) } doReturn true
            on { get(NICKNAME) } doReturn USER
        }

        val time = mock<Time> {
            on { millis() } doReturn TIME
        }

        val idGenerator = mock<IdGenerator> {
            on { next() } doReturn 1
        }

        return WriteTweet(users, tweets, idGenerator, time)
    }

}