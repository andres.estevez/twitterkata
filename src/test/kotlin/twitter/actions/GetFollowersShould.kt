package twitter.actions

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import twitter.domain.User
import twitter.domain.UserNotFoundException
import twitter.domain.Users

class GetFollowersShould {

    private val FOLLOWED_NICKNAME = "ESMERALDA"
    private val FOLLOWED_NAME = "LA RUBIA DE LA ISLA DE LA MUERTE"
    private val FOLLOWER_NICKNAME = "FENIX"

    private val FOLLOWED = User(FOLLOWED_NAME, FOLLOWED_NICKNAME).addFollower(FOLLOWER_NICKNAME)

    private val USERS_WITHOUT_FOLLOWED = mock<Users>(){
        on { exists(any())} doReturn true
        on { get(any()) } doReturn null
    };

    private val USERS_WITH_FOLLOWED = mock<Users>(){
        on { exists(any())} doReturn true
        on { get(any()) } doReturn FOLLOWED
    };

    @Test
    fun `throw exception if followed not found`() {
        // Given
        var getFollowers = GetFollowers(USERS_WITHOUT_FOLLOWED)

        assertThrows<UserNotFoundException> {
            // When
            getFollowers.execute(FOLLOWED_NICKNAME)
        }
    }

    @Test
    fun `return a list of followers`() {
        // Given
        var getFollowers = GetFollowers(USERS_WITH_FOLLOWED)

        // When
        val followers = getFollowers.execute(FOLLOWED_NICKNAME)

        // Then
        assertThat(followers).isEqualTo(listOf(FOLLOWER_NICKNAME))
    }
}