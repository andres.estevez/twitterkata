package twitter.actions

import com.nhaarman.mockitokotlin2.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import twitter.domain.User
import twitter.domain.UserNotFoundException
import twitter.domain.Users

class UpdateNameShould {
    private val NICKNAME = "FENIX"
    private val NAME = "IKKI"
    private val NEWNAME = "JHUN"

    private val UPDATE_NAME_DATA = UpdateNameData(NICKNAME, NEWNAME)

    private val USERS_CONTAINING_NICKNAME = mock<Users>(){
        on { exists(any())} doReturn true
        on { get(any()) } doReturn User(NAME, NICKNAME)
    };

    private val USERS_NOT_CONTAINING_NICKNAME = mock<Users>(){
        on { exists(any())} doReturn false
    }

    @Test
    fun `throw exception if user not found`() {
        // Given
        var updateName = UpdateName(USERS_NOT_CONTAINING_NICKNAME)

        // Then
        assertThrows<UserNotFoundException> {
            // When
            updateName.execute(UPDATE_NAME_DATA)
        }
    }

    @Test
    fun `update user with new name`() {
        // Given
        var updateName = UpdateName(USERS_CONTAINING_NICKNAME)

        // When
        updateName.execute(UPDATE_NAME_DATA)

        // Then
        verifyUserSaved(NICKNAME, NEWNAME);
    }

    private fun verifyUserSaved(nickname: String, name: String) {
        val captor: KArgumentCaptor<User> = argumentCaptor { }
        verify(USERS_CONTAINING_NICKNAME, times(1)).save(captor.capture())
        assertThat(captor.firstValue.nickname).isEqualTo(nickname);
        assertThat(captor.firstValue.name).isEqualTo(name);
    }
}