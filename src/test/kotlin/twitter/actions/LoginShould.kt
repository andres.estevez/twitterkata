package twitter.actions

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import twitter.domain.InvalidCredentialsException
import twitter.domain.Users

class LoginShould {
    private val NICKNAME = "Fenix"
    private val PASSWORD = "esmeralda"
    private val CREDENTIALS = Credentials(NICKNAME, PASSWORD)

    @Test
    fun `throw exception if user not exists`() {
        // Given
        val users = mock<Users> {
            on { exists(any()) } doReturn false
        }
        val login = Login(users)

        assertThrows<InvalidCredentialsException> {
            // When
            login.execute(CREDENTIALS)
        }
    }
}