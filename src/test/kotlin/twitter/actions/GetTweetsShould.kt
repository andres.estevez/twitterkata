package twitter.actions

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import twitter.domain.Tweet
import twitter.domain.Tweets
import twitter.domain.UserNotFoundException
import twitter.domain.Users

class GetTweetsShould {
    private val NICKNAME = "fenix"
    private val TEXT1 = "TEXT1"
    private val TEXT2 = "TEXT2"
    private val ID1 = 1L
    private val ID2 = 2L
    private val TIME1: Long = 1
    private val TIME2: Long = 2
    private val TWEET1 = Tweet(ID1, NICKNAME, TEXT1, TIME1)
    private val TWEET2 = Tweet(ID2, NICKNAME, TEXT2, TIME2)

    @Test
    fun `throw exception if user not found`() {
        // Given
        val getTweets = GetTweets(mock(), mock())

        assertThrows<UserNotFoundException> { getTweets.execute(NICKNAME) }
    }

    @Test
    fun `should return all tweets of a given user`() {
        // Given
        val users = mock<Users> {
            on { exists(NICKNAME) } doReturn true
        }
        val tweets = mock<Tweets> {
            on { getAllOf(NICKNAME) } doReturn listOf(TWEET1, TWEET2)
        }
        val getTweets = GetTweets(users, tweets)

        // When
        val result = getTweets.execute(NICKNAME)

        // Then
        assertThat(result).isEqualTo(listOf(TWEET1, TWEET2))
    }
}